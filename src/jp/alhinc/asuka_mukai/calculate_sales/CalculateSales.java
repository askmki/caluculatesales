package jp.alhinc.asuka_mukai.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {



	public static void main(String[] args) {
		System.out.println("ここにあるファイルを開きます＝＞"+ args[0]);
		Map<String,String>branchmap = new HashMap<String,String>();
		Map<String,Long>salesMap = new HashMap<String,Long>();


		BufferedReader br = null;
		try {
			File branchfile = new File(args[0],"branch.lst");

			if(!branchfile.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}



			FileReader fr = new FileReader(branchfile);
			br=new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null) {
				System.out.println(line);

				String[]arr = line.split(",");


				branchmap.put(arr[0],arr[1]);
				salesMap.put(arr[0],(long)0);

				if(arr.length>=3) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}


				if(!arr[0].matches("^[0-9]{3}$")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}



			}


		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
			return;

		} finally {

			try {
				br.close();
			}catch(IOException e) {
				System.out.println("closeできませんでした。");
				return;


			}
		}



		File salesfile = new File(args[0]);
		File salesfiles[] = salesfile.listFiles();

		for(int i=0; i<salesfiles.length;i++) {
			if(salesfiles[i].getName().matches("^[0-9]{8}.rcd$")) {

				try {
					FileReader fr = new FileReader(salesfiles[i]);
					br = new BufferedReader(fr);
					String line;
					List<String>list = new ArrayList<String>();

					while((line=br.readLine())!=null) {
						//listの中身を入れる

						list.add(line);


					}

					if(list.size()>=3) {
						System.out.println(salesfiles[i].getName()+"のフォーマットが不正です");
						return;
					}

					if(!branchmap.containsKey(list.get(0))) {
						System.out.println(salesfiles[i].getName()+"支店コードが不正です");
						return;

					}




					long added = salesMap.get(list.get(0));
					added += Long.parseLong(list.get(1));
					salesMap.put(list.get(0),added);






					String sum = Long.toString(added);
					if(10 <= sum.length()) {
						System.out.println("合計金額が10桁を超えました");
						return;


					}

				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;

				} finally {
					if(br !=null) {
						try {
							br.close();
						}catch(IOException e) {


						}

					}
				}


			}

			BufferedWriter bw = null;
			try {
				File outputfile = new File(args[0],"branch.out");
				FileWriter fw = new FileWriter(outputfile);
				bw=new BufferedWriter(fw);

				for(String key:salesMap.keySet()) {

					bw.write(key+",");
					bw.write(branchmap.get(key)+",");
					String line = Long.toString(salesMap.get(key));
					bw.write(line);
					bw.newLine();
				}
				bw.close();

			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;

			} finally {
				try {
					bw.close();
				}catch (IOException e) {

				}

			}





		}
	}


}








